@echo off
color A
Title Batch Script Kill Process

pause

taskkill /f /im notepad.exe		:: /im = image name (process name)	::	/f = force close

tasklist | findstr notepad.exe || start notepad.exe

pause

::	0 = black 	8 = gray
::	1 = navy 	9 = blue
::	2 = green 	A	= lime
::	3 = teal 	B = aqua
::	4 = maroon 	C = red
::	5 = purple 	D = fuchsia
::	6 = olive 	E = yellow
::	7 = silver 	F = white
