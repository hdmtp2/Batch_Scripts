```
Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted
```

1. Move the `script1234.cmd` file in the StartUp folder. 
    StartUp folder location = `C:\Users\<user_name>\AppData\Roaming\Microsoft\Windows\Strat Menu\Programs\Startup`

2. Use `whoami` comand to know the username

3. `Move-Item -Path <pwd>\script1234.cmd -Destination <StartUp folder location>`

4. Put `script1234.ps1` file location in the script1234.cmd file.

5. Wrote all the commands inside `try` `catch` block. So It can be used in any Windows machine without the script stopping due to errors

6. `pip install requests`
