import requests as r
import time
import sys
import os

bot_token = "6001518602:AAHqV1PnjaNfgz5_4BPKuc8-d41rE2M-V_4"
chat_id = 5201795863
img = sys.argv[2]
#doc = "MOUSUMI.pptx"
doc = sys.argv[2]

def sendImg(path : str):
    url = f"https://api.telegram.org/bot{bot_token}/sendPhoto"
    img_data = {"chat_id" : chat_id, "caption" : time.ctime(time.time())}
    with open(path, "rb") as img_file:
        r.post(url, data = img_data, files = {"photo" : img_file})

def sendDoc(path: str):
    url = f"https://api.telegram.org/bot{bot_token}/sendDocument"
    doc_data = {"chat_id" : chat_id}
    with open(path, "rb") as doc_file:
        r.post(url, data = doc_data, files = {"document" : doc_file})

if __name__ == "__main__" : 
    if sys.argv[1] == 0:
        sendImg(img)
    else:
        sendDoc(doc)
