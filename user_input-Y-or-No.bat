@echo off

:start
cls
set /p user_input = Will you like to continue (y/n)? : 
if not defined user_input goto start
if /i %user_input% == y goto Yes		:: /i = NOT case sensitive (takes both 'n' & 'N')
if /i %user_input% == n (goto No) else (goto Invalid)


:Yes
echo user has entered Yes
pause
goto start 

:No
echo user has selected No
pause
exit

:Invalid
echo Invalid input!!!
set user_input =
pause
goto start
