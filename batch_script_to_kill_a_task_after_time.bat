@echo off
color A
Title Batch script to kill a process after sometime
start notepad.exe

timeout /t 5 /nobreak	::	/nobreak = dosent get interrupted and closed by keyboard input	::	/t = <timeoutinseconds>

taskkill /f /im notepad.exe
pause
