param (
	[string]$ext
)

$startDir = "C:\"

$dllFiles = Get-ChildItem -Path $startDir -Recurse -File -Filter $ext

foreach ($file in $dllFiles) { Write-Host $file.FullName }
